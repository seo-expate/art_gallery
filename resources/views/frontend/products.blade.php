@extends('frontend.layout')
@section('content')

<!--===========================all_products part start===================================-->
<section class="all_products">
		<div class="container">
			<div class="row">
				<aside class="col-md-3 mb-5">				
					<div class="card">
						<article class="filter-group Product_categories">
							<header class="card-header">
								<i class="icon-control fa fa-chevron-down"></i>
								<h6 class="title">Product categories</h6>
							</header>
							<div class="filter-content">
								<div class="card-body">
									<form class="pb-3">
										<div class="input-group">
										  	<input type="text" class="form-control" placeholder="Search">
										  	<div class="input-group-append">
										    	<button class="btn btn-light" type="button"><i class="fa fa-search"></i></button>
										  	</div>
										</div>
									</form>									
									<ul class="navbar-nav ms-auto ">
										<li ><a href="#">Blender Accessories</a></li>
										<li><a href="#">Hand Blenders</a></li>
										<li><a href="#">Portable Blender</a></li>
										<li><a href="#">Coffee Makers</a></li>
										<li><a href="#">Stand Mixers</a></li>
										<li><a href="#">Hands-On Espresso</a></li>
										<li><a href="#">Manual Espresso Makers</a></li>
										<li><a href="#">Espresso Machines</a></li>
										<li><a href="#">Automatic Espresso</a></li>
										<li><a href="#">Tilt-Head Stand Mixers</a></li>
										<li><a href="#">Bowl-Lift Stand Mixers</a></li>
										<li><a href="#">Mixer Attachment</a></li>
									</ul>
								</div> 
							</div>
						</article> 
						<article class="filter-group">
							<header class="card-header">
								<i class="icon-control fa fa-chevron-down"></i>
								<h6 class="title">Product Brands </h6>
							</header>
							<div class="filter-content">
								<div class="card-body">
									<label class="d-flex justify-content-between">
									  <div>
									  	<input type="checkbox">
									  	<span >Espresso </span>
									  </div>
									  <div><span>120</span></div>
									</label>
									<label class="d-flex justify-content-between">
									  <div>
									  	<input type="checkbox">
									  	<span >Blender </span>
									  </div>
									  <div><span>50</span></div>
									</label>
									<label class="d-flex justify-content-between">
									  <div>
									  	<input type="checkbox">
									  	<span >Coffee </span>
									  </div>
									  <div><span>40</span></div>
									</label>
									<label class="d-flex justify-content-between">
									  <div>
									  	<input type="checkbox">
									  	<span >Mixer </span>
									  </div>
									  <div><span>80</span></div>
									</label>
									<label class="d-flex justify-content-between">
									  <div>
									  	<input type="checkbox">
									  	<span >Tilt-Head </span>
									  </div>
									  <div><span>30</span></div>
									</label>
								</div> 
							</div>
						</article> 
						<article class="filter-group filter_price">
							<header class="card-header">
								<i class="icon-control fa fa-chevron-down"></i>
								<h6 class="title">Price range </h6>
							</header>
							<div class="filter-content ">
								<div class="card-body">
									<input type="range" >
									<div class="form-row d-flex mb-4 gap-2">
										<div class="form-group col-md-6">
										  	<label>Min</label>
										  	<input class="form-control" placeholder="$0" type="number">
										</div>
										<div class="form-group text-right col-md-6">
										  	<label>Max</label>
										  	<input class="form-control" placeholder="$1,0000" type="number">
										</div>
									</div> 
									<button class="btn">Filter</button>
								</div>
							</div> 
						</article> 
						<article class="filter-group">
							<header class="card-header">
								<i class="icon-control fa fa-chevron-down"></i>
								<h6 class="title">Product Sizes </h6>
							</header>
							<div class="filter-content">
								<div class="card-body">
								  	<label class=" d-block">
								    	<input type="checkbox">
								    	<span> XS </span>
								  	</label>
								  	<label class="d-block">
								    	<input type="checkbox">
								    	<span> SM </span>
								  	</label>
								  	<label class="d-block">
								    	<input type="checkbox">
								    	<span> LG </span>
								  	</label>
								  	<label class="d-block">
								    	<input type="checkbox">
								    	<span> XXL </span>
								  	</label>
								  	<label class="d-block">
								    	<input type="checkbox">
								    	<span> XL </span>
								  	</label>
								</div>
							</div>
						</article> 
						<article class="filter-group">
							<header class="card-header">
								<i class="icon-control fa fa-chevron-down"></i>
								<h6 class="title">More filters </h6>
							</header>
						</article> 
					</div>
				</aside>
				<main class="col-md-9 our_best_collection Related_Products all_product_gallerys">
					<header class="border-bottom mb-4 pb-3">
						<div class="form-inline">
							<span>{{ $total_products }} Items found </span>
							<select class="mr-2 form-control mt-2">
								<option>Latest items</option>
								<option>Trending</option>
								<option>Most Popular</option>
								<option>Cheapest</option>
							</select>
						</div>
					</header>
					<div class="row">
                        @foreach($arr as $product)
						<div class="col-md-4">
							 <figure class="card card-product-grid">
								<div class="card p-0">
						        	<div class="card-header">
						        		<a href="{{ route('shop.details', ['slug' => $product['slug']])}}"><img src="{{ asset($product['image']) }}" alt="img"></a>
						        	</div>	          
						          <div class="card-body">
						            <div class="rating  col-md-12 col-xl-12">
						            	<p>{{ $product['regular_price'] }}$</p>
									    <div class="rat col-md- col-xl-">
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star-half-stroke"></i>				        
									        <i class="fa-regular fa-star"></i>
									    </div>
									    <p>{{ $product['name'] }}</p>
									    <p>Dimension : {{ $product['size'] }}</p>
									</div>
						          </div>
						          <div class="woocommerce_cart">
						          	<a href="javascript:void(0)" onclick="addToCart({{ $product['id'] }})">
										<div class="woo_cart">
											<i class="fa-solid fa-cart-plus"></i>
										</div>
									</a>
						          	<div class="woo_cart">
						          		<i class="fa-regular fa-heart"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-layer-group"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
						          	</div>
						          </div>
						        </div>
							</figure>
						</div>  
						@endforeach
					</div> 
					<nav class="mt-4 cat_pagination" aria-label="Page navigation sample">
					  <ul class="pagination">
					    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
					    <li class="page-item active"><a class="page-link" href="#">1</a></li>
					    <li class="page-item"><a class="page-link" href="#">2</a></li>
					    <li class="page-item"><a class="page-link" href="#">3</a></li>
					    <li class="page-item"><a class="page-link" href="#">Next</a></li>
					  </ul>
					</nav>
				</main>
			</div>
		</div>
	</section>
	<!--===========================all_products part end===================================-->
	<script>
			
		function addToCart(id){
			$.ajax({
				url : "{{ route('front.addToCard') }}",
				type : "post",
				dataType : 'json',
				data : {
					"_token": "{{ csrf_token() }}",
					"id" : id
				},

				success: function(res){

					if(res.status == true){
						window.location.href= "{{ route('front.cart') }}";
					}else{
						alert(res.message);
					}
					
				}
			});
		}

	</script>

@endsection

