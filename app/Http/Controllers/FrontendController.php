<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use Illuminate\Support\Facades\Request;

class FrontendController extends Controller
{


    public function index(){
        return view('frontend.home');
    }

    public function products(){
        $arr = ProductModel::orderBy('id', 'DESC')->get();    
        $total_products = $arr->count();
        return view('frontend.products', compact('arr', 'total_products'));
    }

    public function product_details(){

        $slug = Request::segment(count(Request::segments()));        
        //$product = ProductModel::where('slug', $slug)->first();
        $product = ProductModel::with('category', 'architect')->where('slug', $slug)->first();
        $other_images = json_decode($product['otherImage']);        
        
        //$calculated = $product['selling_price']/$product['regular_price']*100;
         
        $calculated = ($product['regular_price']-$product['selling_price'])/$product['regular_price'] * 100;
        $percentage = number_format($calculated,2);
        

        return view('frontend.products_details', compact('product', 'other_images', 'percentage'));
    }
}
