@extends('frontend.layout')
@section('content')

<section class=" user_register_form">
  <div class="container py-5">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-xl-10">
        <div class="card rounded-3 text-black">
          <div class="row g-0">
            <div class="col-lg-6">
              <div class="card-body p-md-5 mx-md-4">
                <div class="text-center">
                  <img src="{{asset('assets/art_design/images/comlogo.png')}}" alt="logo">
                  <h4 class="mt-1 mb-5 pb-1">Welcome To Login </h4>
                </div>
                <form method="post" action="{{ route('customer.login')}}">
                  {!! csrf_field() !!}
                  <p>Please login to your account</p>
                  <div class="form-outline mb-4">
                    <label class="form-label" for="user">Username</label>
                    <input type="email" name="email" id="user" class="form-control" placeholder="Phone number or email address" />
                  </div>
                  <div class="form-outline mb-4">
                    <label class="form-label" for="pass">Password</label>
                    <input type="password" name="password" id="pass" class="form-control" />
                  </div>
                  <div class="text-center pt-1 mb-5 pb-1 loginbtn">
                    <button class="btn mb-3" type="submit">Log in</button>
                    <a class="text-muted" href="#!">Forgot password?</a>
                  </div>                  
                </form>
                <div class="d-flex align-items-center justify-content-center pb-4">
                    <p class="mb-0 me-2">Don't have an account?</p>
                    <a href="register.html"><button type="button" class="btn btn-outline-danger">Create new</button></a>
                  </div>
              </div>
            </div>
            <div class="col-lg-6 d-flex align-items-center btnblock">
              <div class="text-white px-3 py-4 p-md-5 mx-md-4">
                <h4 class="mb-4">We are more than just a company</h4>
                <p class="small mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                  exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection