<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BLogModel extends Model
{
    protected $table = 'blogs';
    protected $primaryKey = 'id';
    use HasFactory;
    protected $guarded=[];

    public function category()
    {
        return $this->belongsTo(BlockCategoryModel::class, 'category_id', 'id');
    }
}
