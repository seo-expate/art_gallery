<?php

use App\Http\Controllers\ArchitectController;
use App\Http\Controllers\BlockCategoryController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [FrontendController::class, 'index'])->name('home');
Route::get('/shop', [FrontendController::class, 'products'])->name('shop');
Route::get('shop/{slug}', [FrontendController::class, 'product_details'])->name('shop.details');
Route::get('cart/', [CartController::class, 'cart'])->name('front.cart');
Route::post('add-to-cart/', [CartController::class, 'addToCart'])->name('front.addToCard');
Route::post('update-cart/', [CartController::class, 'updateCart'])->name('front.updateCart');
Route::post('deleteItem/', [CartController::class, 'deleteItem'])->name('front.deleteItem');
Route::post('addToCartWithQty/', [CartController::class, 'addToCartWithQty'])->name('front.addToCartWithQty');
Route::get('checkout/', [CartController::class, 'checkout'])->name('front.checkout');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::resource('product', ProductController::class);
    Route::resource('architect', ArchitectController::class);
    Route::resource('category', CategoryController::class);
    Route::resource('blog', BlogController::class);   
    Route::resource('blog_category', BlockCategoryController::class);   
});

Route::resource('customer', CustomerController::class);
Route::post('/customer-login',[CustomerController::class,'customerLogin'])->name('customer.login');


Route::middleware('customerAuth')->group(function(){
    Route::get('/customer_dashboard', [CustomerController::class, 'customerDashboard'])->name('customer_dashboard');
});