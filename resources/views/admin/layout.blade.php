<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Dashboard - NiceAdmin Bootstrap Template</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon"> -->

    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/quill/quill.snow.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/quill/quill.bubble.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
</head>

<body>
    
    @livewire('navigation-menu')
    <aside id="sidebar" class="sidebar">

        <ul class="sidebar-nav" id="sidebar-nav">

            <li class="nav-item">
                <a class="nav-link <?= (Request::segment(1) == 'dashboard')? '' : 'collapsed';?>" href="/dashboard">
                    <i class="bi bi-grid"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= (Request::segment(1) == 'architect')? '' : 'collapsed';?>" data-bs-target="#architect-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Architect</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="architect-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li><a href="{{route('architect.create')}}"><i class="bi bi-circle"></i><span>Add</span></a></li>
                    <li><a href="{{route('architect.index')}}"><i class="bi bi-circle"></i><span>List</span></a></li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= (Request::segment(1) == 'category')? '' : 'collapsed';?>" data-bs-target="#category-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Category</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="category-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li><a href="{{route('category.create')}}"><i class="bi bi-circle"></i><span>Add</span></a></li>
                    <li><a href="{{route('category.index')}}"><i class="bi bi-circle"></i><span>List</span></a></li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= (Request::segment(1) == 'product')? '' : 'collapsed';?>" data-bs-target="#product-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Product</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="product-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li><a href="{{route('product.create')}}"><i class="bi bi-circle"></i><span>Add</span></a></li>
                    <li><a href="{{route('product.index')}}"><i class="bi bi-circle"></i><span>List</span></a></li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= (Request::segment(1) == 'blog')? '' : 'collapsed';?>" data-bs-target="#blog-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Blog</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="blog-nav" class="nav-content collapse " data-bs-parent="#blog-nav">
                    <li><a href="{{route('blog.create')}}"><i class="bi bi-circle"></i><span>Add</span></a></li>
                    <li><a href="{{route('blog.index')}}"><i class="bi bi-circle"></i><span>List</span></a></li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= (Request::segment(1) == 'blog_category')? '' : 'collapsed';?>" data-bs-target="#catBlog-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Blog Category</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="catBlog-nav" class="nav-content collapse " data-bs-parent="#catBlog-nav">
                    <li><a href="{{route('blog_category.create')}}"><i class="bi bi-circle"></i><span>Add</span></a></li>
                    <li><a href="{{route('blog_category.index')}}"><i class="bi bi-circle"></i><span>List</span></a></li>
                </ul>
            </li>

            
            
            

        </ul>

    </aside>

    @yield('content')

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">
        <div class="copyright">
            &copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </footer>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <script src="{{asset('assets/vendor/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/vendor/chart.js/chart.umd.js')}}"></script>
    <script src="{{asset('assets/vendor/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/vendor/quill/quill.min.js')}}"></script>
    <script src="{{asset('assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
    <script src="{{asset('assets/vendor/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>

    <script src="{{asset('assets/js/main.js')}}"></script>

</body>

</html>