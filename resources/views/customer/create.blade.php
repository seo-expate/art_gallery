@extends('frontend.layout')
@section('content')
<!--===========================register part start===================================-->
	
<section class="user_register_form">
	 <div class="bg-light py-3 py-md-5">
	  <div class="container">
	    <div class="row justify-content-md-center">
	      <div class="col-12 col-md-11 col-lg-8 col-xl-7 col-xxl-6">
	        <div class="bg-white p-4 p-md-5 rounded shadow-sm">
	          <div class="row">
	            <div class="col-12">
	              <div class="text-center mb-5">
	                <a href="#!">
	                  <img src="{{asset('assets/art_design/images/comlogo.png')}}" alt="img" width="185px">
	                </a>
	              </div>
	            </div>
	          </div>
	          <form action="{{ route('customer.store') }}"  method="post">
              @csrf
	            <div class="row gy-3 gy-md-4 overflow-hidden">
	              <div class="col-12">
	                <label for="firstName" class="form-label">First Name <span class="text-danger">*</span></label>
	                <div class="input-group">
	                  <span class="input-group-text">
	                  	<i class="fa-regular fa-address-card"></i>
	                  </span>
	                  <input type="text" class="form-control" name="firstName" id="firstName" required>
	                </div>
                  <span class="text-danger">{{ $errors->first('firstName') }}</span>
	              </div>
	              <div class="col-12">
	                <label for="lastName" class="form-label">Last Name <span class="text-danger">*</span></label>
	                <div class="input-group">
	                  <span class="input-group-text">
	                  	<i class="fa-regular fa-credit-card"></i>
	                  </span>
	                  <input type="text" class="form-control" name="lastName" id="lastName" required>
	                </div>
                  <span class="text-danger">{{ $errors->first('lastName') }}</span>
	              </div>
	              <div class="col-12">
	                <label for="email" class="form-label">Email <span class="text-danger">*</span></label>
	                <div class="input-group">
	                  <span class="input-group-text">
	                  	<i class="fa-regular fa-envelope"></i>
	                  </span>
	                  <input type="email" class="form-control" name="email" id="email" required>
	                </div>
                  <span class="text-danger">{{ $errors->first('email') }}</span>
	              </div>
	              <div class="col-12">
	                <label for="password" class="form-label">Password <span class="text-danger">*</span></label>
	                <div class="input-group">
	                  <span class="input-group-text">
	                    <i class="fa-solid fa-key"></i>
	                  </span>
	                  <input type="password" class="form-control" name="password" id="password" value="" required>
	                </div>
                  <span class="text-danger">{{ $errors->first('password') }}</span>
	              </div>
	              <div class="col-12">
	                <div class="form-check">
	                  <input class="form-check-input" type="checkbox" value="" name="iAgree" id="iAgree" required>
	                  <label class="form-check-label text-secondary" for="iAgree">
	                    I agree to the <a href="#!" class="text-decoration-none">terms and conditions</a>
	                  </label>
	                </div>
	              </div>
	              <div class="col-12">
	                <div class="d-grid">
	                  <button class="btn btn-lg" type="submit">Sign Up</button>
	                </div>
	              </div>
	            </div>
	          </form>
	          <div class="row">
	            <div class="col-12">
	              <hr class="mt-5 mb-4 border-secondary-subtle">
	              <p class="m-0 text-secondary text-center">Already have an account? <a href="loin.html" class="text-decoration-none">Sign in</a></p>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	 </div>
	</section>				

	<!--===========================register part end===================================-->
  @endsection