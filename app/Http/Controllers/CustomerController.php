<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        Auth::logout();
        if(Session::get('role') == '2'){            
            return redirect('customer_dashboard');
        }else{
            return view('customer.index');
        }
       
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        request()->validate([
            'firstName' => 'required|min:3|regex:/^[A-Za-z_-]/',          
            'lastName' => 'required|min:3|regex:/^[A-Za-z_-]/',          
            'email' => 'required|min:3|unique:customers',
            'password' => 'required|min:6',            
            // 'password_confirmation'=> 'min:6|same:password|required_with:password',
        ]);
        //dd($input);

        if(isset($input['image'])){
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('images/Architect', $file_name, 'public');
            $input['image'] = "storage/images/Architect/".$file_name;

            if(file_exists($input['old_img'])){
                unlink($input['old_img']);
            }

        }   
                
        $input['password'] = Hash::make($input['password']);

        unset($input['_token'], $input['iAgree']);

        CustomerModel::create($input);
        return redirect('customer_dashboard');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $details = CustomerModel::find($id);
        $input = $request->all();    
          
        
        if (array_key_exists("current_password",$input)){

            $user = CustomerModel::find($input['id']);
            
            $request->validate([
                'current_password' => 'required',
                'new_password' => 'required|confirmed|',
                'new_password_confirmation' => 'required|same:new_password|different:current_password',
            ]);

            #Match The Old Password
            if(Hash::check($input['current_password'], $user['password'])){
                $new_password['password'] = Hash::make($input['new_password']);
                $details->update($new_password);
                return redirect('customer_dashboard')->with('flash_message', 'Password has been changed');
            }else{
                return redirect('customer_dashboard')->with('error', 'Password current password not correct');
            }



        }


        request()->validate([
            'name' => 'min:3|regex:/^[A-Za-z_-]/',          
            'phone' => 'required|numeric|digits_between:8,15',
            'email' => '',
            'date_of_birth' => 'string|min:3|max:64|nullable',
            'gender' => 'string|min:4|max:10|nullable',
            'street_address' => 'string|min:3|max:64|nullable',
            'city' => 'string|min:3|max:64|nullable',
            'state' => 'string|min:3|max:64|nullable',
            'post' => 'string|min:3|max:64|nullable',
            'country' => 'string|min:3|max:64|nullable',
            'ssn' => 'string|min:3|max:64|nullable',
            'company' => 'string|min:3|max:64|nullable',
            'website' => 'string|min:3|max:255|nullable',
            'facebook' => 'string|min:3|max:255|nullable',
            'linkedIn' => 'string|min:3|max:255|nullable',
            'twitter' => 'string|min:3|max:255|nullable',
            'youtube' => 'string|min:3|max:255|nullable',
            'instagram' => 'string|min:3|max:255|nullable',
            'marital_status' => 'min:1|max:1|nullable'
        ]);

        if(isset($input['image'])){
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('images/customers', $file_name, 'public');
            $input['image'] = "storage/images/customers/".$file_name;
            
            if(file_exists($input['old_img'])){
                unlink($input['old_img']);
            }

        }   


        unset($input['_token']);
        unset($input['old_img']);

        $details->update($input);
        return redirect('customer_dashboard')->with('flash_message', 'Updated');

        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function customerDashboard(){
        $data = CustomerModel::find(Session::get('id'));       
        return view('customer.dashboard')->with('data', $data);
    }

    public function customerLogin(Request $request){        
        
        $input = $request->all();    
        //dd($input);
        $user = CustomerModel::where('email','=',$input['email'])->first();        

        request()->validate([
            'email' => 'required|min:3',
            'password' => 'required',            
        ]);

        $user['role'] = '2';

        if(isset($user['email'])){
            
            Session::put([
                'id' => $user['id'],
                'name' => $user['name'],
                'firstName' => $user['firstName'],
                'role' => $user['role']
            ]);

            return redirect('customer_dashboard');

           
        }else{
            return redirect('customer')->with('error', 'Not matched with customer');
        }


    }
}
