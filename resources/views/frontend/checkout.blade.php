@extends('frontend.layout')
@section('content')

<!--===========================check_out part start===================================-->

<section class="check_out">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mb-4">
                <div class="card mb-4">
                    <div class="card-header py-3">
                        <h5 class="mb-0">Biling details</h5>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row mb-4">
                                <div class="col">
                                    <div class="form-outline">
                                        <label class="form-label" for="fname">First name</label>
                                        <input type="text" id="fname" class="form-control" />
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-outline">
                                        <label class="form-label" for="lname">Last name</label>
                                        <input type="text" id="lname" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="cname">Company name</label>
                                <input type="text" id="cname" class="form-control" />
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="address">Address</label>
                                <input type="text" id="address" class="form-control" />
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="email">Email</label>
                                <input type="email" id="email" class="form-control" />
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="phone">Phone</label>
                                <input type="number" id="phone" class="form-control" />
                            </div>
                            <hr class="my-4" />
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="checkformone" />
                                <label class="form-check-label" for="checkformone">
                                    Shipping address is the same as my billing address
                                </label>
                            </div>
                            <div class="form-check mb-4">
                                <input class="form-check-input" type="checkbox" value="" id="checkoutForm2" checked />
                                <label class="form-check-label" for="checkoutForm2">Save this information for next time</label>
                            </div>
                            <hr class="my-4" />
                            <h5 class="mb-4">Payment</h5>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="checkoutForm3" checked />
                                <label class="form-check-label" for="checkoutForm3">Credit card</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="checkoutForm4" />
                                <label class="form-check-label" for="checkoutForm4">Debit card</label>
                            </div>
                            <div class="form-check mb-4">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="checkoutForm5" />
                                <label class="form-check-label" for="checkoutForm5"> Paypal </label>
                            </div>
                            <div class="row mb-4">
                                <div class="col">
                                    <div class="form-outline">
                                        <label class="form-label" for="formNameOnCard">Name on card</label>
                                        <input type="text" id="formNameOnCard" class="form-control" />
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-outline">
                                        <label class="form-label" for="formCardNumber">Credit card number</label>
                                        <input type="text" id="formCardNumber" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3">
                                    <div class="form-outline">
                                        <label class="form-label" for="formExpiration">Expiration</label>
                                        <input type="text" id="formExpiration" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-outline">
                                        <label class="form-label" for="formCVV">CVV</label>
                                        <input type="text" id="formCVV" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-lg btn-block" type="submit">
                                Continue to checkout
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card mb-4">
                    <div class="card-header py-3">
                        <h5 class="mb-0">Summary</h5>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                                Products
                                <span>$53.98</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                Shipping
                                <span>Gratis</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                                <div>
                                    <strong>Total amount</strong>
                                    <strong>
                                        <p class="mb-0">(including VAT)</p>
                                    </strong>
                                </div>
                                <span><strong>$53.98</strong></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--===========================check_out part end===================================-->

<!--===========================client_logo Products part start===================================-->
<section class="client_logo text-center pb-5 pt-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <img src="images/Brand-2.png" alt="img">
            </div>
            <div class="col-sm-3">
                <img src="images/Brand-3.png" alt="img">
            </div>
            <div class="col-sm-3">
                <img src="images/Brand-1.png" alt="img">
            </div>
            <div class="col-sm-3">
                <img src="images/Brand-4.png" alt="img">
            </div>
        </div>
    </div>
</section>
<!--===========================client_logo Products part end===================================-->

@endsection