@extends('frontend.layout')
@section('content')

<!--===========================product_details part start===================================-->
<section class="product_details">
	<div class="container">

		@if(isset($other_images))
		<div class="lightbox" id="lightbox">
			<div class="lightbox-content">
				<span class="close cursor" onclick="closeModal()">&times;</span>

				@foreach($other_images as $other_img)
				<div class="mySlides">
					<img src="{{asset($other_img)}}">
				</div>
				@endforeach

				<a href="#" class="prev" onclick="plusSlides(-1)">&#10094;</a>
				<a href="#" class="next" onclick="plusSlides(1)">&#10095;</a>
			</div>
			<div class="thumbnails cursor">
				@foreach($other_images as $other_img)
				<img src="{{asset($other_img)}}" alt="" onclick="openModal();currentSlide(1)">
				@endforeach
			</div>
		</div>
		@endif
		<div class="row">
			<div class="imgs-col col-md-6">
				<div class="main-img">
					<a href="#" class="prev2" onclick="plusSlides2(-1)">&#10094;</a>
					<div class="mySlides2">
						<img src="{{asset($product['image'])}}">
					</div>
					<a href="#" class="next2" onclick="plusSlides2(1)">&#10095;</a>
				</div>
					@if(isset($other_images))
					<div class="thumbnails cursor">
						@foreach($other_images as $other_img)
						<img src="{{asset($other_img)}}" alt="" class="" onclick="openModal();currentSlide(1)">
						@endforeach
					</div>
					@endif
			</div>
			<div class="text-col col-md-6">
				<span id="highlight">lorem Ipsum</span>
				<h2>{{ $product['name'] }}</h2>
				<p>{{ $product['short_description'] }}</p>
				<div class="rat col-md- col-xl-">
					<i class="fa-solid fa-star"></i>
					<i class="fa-solid fa-star"></i>
					<i class="fa-solid fa-star"></i>
					<i class="fa-solid fa-star-half-stroke"></i>
					<i class="fa-regular fa-star"></i>
				</div>
				<div class="d-flex gap-5">
					<span>SKU : </span>
					<span> {{ $product['product_code'] }}</span>
				</div>
				<div class="d-flex gap-5">
					<span>Category : </span>
					<span> {{ $product->category->name }}</span>
				</div>
				<div class="d-flex gap-5">
					<span>Tags : </span>
					<span> {{ $product->architect->name }}</span>
				</div>
				<span id="price">
					<h2>$<span id="amount">{{ $product['selling_price'] }}</span></h2>
					<small>{{ $percentage }}%</small>
				</span>
				<span id="old-price">${{ $product['regular_price'] }}</span>
				<p class="text-success">{{ $product['stock_amount'] }} in stock</p>
				<div class="options">
					<button><img src="{{asset('assets/art_design/images/icon-minus.svg')}}" alt="" onclick="minus()"></button>
					<button id="result">1</button>
					<button onclick="plus()"><img src="{{asset('assets/art_design/images/icon-plus.svg')}}" alt=""></button>
					<button id="addToCart" onclick="addToCartWithQty('{{ $product['id']}}')"><i class="fa-solid fa-cart-plus"></i>Add to Cart</button>
				</div>
			</div>
		</div>
	</div>
</section>
<!--===========================product_details part end===================================-->

<!--===========================product_description part start===================================-->
<section class="product_description ">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div style=" padding: 10px; margin: 10px">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item" role="presentation">
							<button class="nav-link active" id="personal-tab" data-bs-toggle="tab" data-bs-target="#personal" type="button" role="tab" aria-controls="personal" aria-selected="true">
								Description</button>
						</li>
						<li class="nav-item" role="presentation">
							<button class="nav-link" id="employment-tab" data-bs-toggle="tab" data-bs-target="#employment" type="button" role="tab" aria-controls="employment" aria-selected="false">
								Additional information</button>
						</li>
						<li class="nav-item" role="presentation">
							<button class="nav-link" id="Reviews-tab" data-bs-toggle="tab" data-bs-target="#Reviews" type="button" role="tab" aria-controls="Reviews" aria-selected="false">
								Reviews (1)</button>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="personal" role="tabpanel" aria-labelledby="personal-tab">
						{!! $product['long_description'] !!}
						</div>
						<div class="tab-pane fade" id="employment" role="tabpanel" aria-labelledby="employment-tab">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th>Weight </th>
										<td>{{ $product['weight'] }}</td>
									</tr>
									<tr>
										<th>Dimensions</th>
										<td>{{ $product['size'] }}</td>
									</tr>									
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="Reviews" role="tabpanel" aria-labelledby="Reviews-tab">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th><img src="{{asset('assets/art_design/images/images.png')}}" width="50px" alt="img"></th>
										<td>
											<div class="rat col-md- col-xl-">
												<i class="fa-solid fa-star"></i>
												<i class="fa-solid fa-star"></i>
												<i class="fa-solid fa-star"></i>
												<i class="fa-solid fa-star-half-stroke"></i>
												<i class="fa-regular fa-star"></i>
											</div>
											<p>admin – December 28, 2016</p>
											<p>Love it. Works great . I would give 5 stars if it supports picture uploads.</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--===========================product_description part end===================================-->

<!--===========================Our Best Collection part start===================================-->

<section class="our_best_collection Related_Products">
	<div class="container">
		<div class="row mt-5" id="filter-buttons">
			<h2>Related Products</h2>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem, commodi?</p>
			<div class="col-12 mt-4">
				<button class="btn mb-2 me-1 active" data-filter="all">Show all</button>
				<button class="btn mb-2 mx-1" data-filter="Coffee">Coffee Makers</button>
				<button class="btn mb-2 mx-1" data-filter="Kettles">Kettles</button>
				<button class="btn mb-2 mx-1" data-filter="Slicer">Slicer</button>
			</div>
		</div>
		<hr>
		<div class="row px-2 mt-4 gap-3" id="filterable-cards">
			<div class="card p-0" data-name="Coffee">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (1).jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Coffee">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (2).jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Coffee">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (3).jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Kettles">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (6).jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Kettles">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/11.jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Kettles">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (5).jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Kettles">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (7).jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Slicer">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (9).jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Slicer">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/2.jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
			<div class="card p-0" data-name="Slicer">
				<div class="card-header">
					<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (10).jpg')}}" alt="img"></a>
				</div>
				<div class="card-body">
					<div class="rating  col-md-12 col-xl-12">
						<p>100$</p>
						<div class="rat col-md- col-xl-">
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star"></i>
							<i class="fa-solid fa-star-half-stroke"></i>
							<i class="fa-regular fa-star"></i>
						</div>
						<p>laborum et dolorum fug </p>
						<p>Lorem ipsum, dolor sit amet.</p>
					</div>
				</div>
				<div class="woocommerce_cart">
					<div class="woo_cart">
						<i class="fa-solid fa-cart-plus"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-regular fa-heart"></i>
					</div>
					<div class="woo_cart">
						<i class="fa-solid fa-layer-group"></i>
					</div>
					<div class="woo_cart">
						<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--===========================Our Best Collection part end===================================-->

<!--===========================client_logo Products part start===================================-->
<section class="client_logo text-center pb-5 pt-5">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<img src="{{asset('assets/art_design/images/Brand-2.png')}}" alt="img">
			</div>
			<div class="col-sm-3">
				<img src="{{asset('assets/art_design/images/Brand-3.png')}}" alt="img">
			</div>
			<div class="col-sm-3">
				<img src="{{asset('assets/art_design/images/Brand-1.png')}}" alt="img">
			</div>
			<div class="col-sm-3">
				<img src="{{asset('assets/art_design/images/Brand-4.png')}}" alt="img">
			</div>
		</div>
	</div>
</section>
<!--===========================client_logo Products part end===================================-->

<script>
	//this function define the product_details page popup modal
	function openModal() {
		document.getElementById("lightbox").style.display = "flex";
	}

	function closeModal() {
		document.getElementById("lightbox").style.display = "none";
	}
	//this function define the product_details page popup product-slider
	var slideIndex = 1;
	showSlides(slideIndex);

	function plusSlides(n) {
		showSlides(slideIndex += n);
	}

	function currentSlide(n) {
		showSlides(slideIndex = n);
	}

	function showSlides(n) {
		var i;
		var slides = document.getElementsByClassName("mySlides");
		if (n > slides.length) {
			slideIndex = 1
		}
		if (n < 1) {
			slideIndex = slides.length
		}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}

		slides[slideIndex - 1].style.display = "flex";
	}
	//this function define the product_details page product-slider
	var slideIndex2 = 1;
	showSlides2(slideIndex2);

	function plusSlides2(n) {
		showSlides2(slideIndex2 += n);
	}

	function currentSlide2(n) {
		showSlides2(slideIndex2 = n);
	}

	function showSlides2(n) {
		var i;
		var slides = document.getElementsByClassName("mySlides2");
		if (n > slides.length) {
			slideIndex2 = 1
		}
		if (n < 1) {
			slideIndex2 = slides.length
		}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}

		slides[slideIndex2 - 1].style.display = "flex";
	}

	function addToCartWithQty(id){
			var qty = parseInt($("#result").text());
			
			$.ajax({
				url : "{{ route('front.addToCartWithQty') }}",
				type : "post",
				dataType : 'json',
				data : {
					"_token": "{{ csrf_token() }}",
					"id" : id,
					'qty'   : qty
				},

				success: function(res){

					if(res.status == true){
						window.location.href= "{{ route('front.cart') }}";
					}else{
						alert(res.message);
					}
					
				}
			});
	}


</script>
@endsection