<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {    
        $arr = ProductModel::with('category', 'architect')->get();        
        return view('admin.product.index', compact('arr'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories_arr = DB::table('categories')->where('status','=',1)->get();
        $architect_arr = DB::table('architects')->where('status','=',1)->get();   
        return view('admin.product.create', compact('categories_arr', 'architect_arr'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {       
        $input = $request->all();
        
        request()->validate([
            'name' => 'required|max:200|min:3|regex:/^[A-Za-z_-]/', 
            'size' => 'required|max:20|min:3|', 
            'regular_price' => 'required|numeric', 
            'selling_price' => 'required|numeric',
            'stock_amount' => 'required|numeric',
            'short_description' => 'required',
            'long_description' => 'required',
            'slug' => 'required|unique:products',
            'product_code' => 'required|unique:products', 
            'product_materials' => 'required', 
            'architect_id' => 'required', 
            'category_id' => 'required'
        ]);
        
        $str = $input['name'];
        $delimiter = '-';
        $input['slug'] = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        

        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('images/Products', $file_name, 'public');
            $input['image'] = "storage/images/Products/".$file_name;
        }else{
            $input['image'] = '';
        }

        

        if(isset($input['otherImage']))
        {
            $names = [];
            foreach($request->file('otherImage') as $image)
            {
                $destinationPath = 'storage/images/other_Products/';
                $filename = $destinationPath.time().$image->getClientOriginalName();
                $image->move($destinationPath, $filename);
                array_push($names, $filename);
            }

            $input['otherImage'] = json_encode($names);
        }else{
            $input['otherImage'] = '';
        }

        


        unset($input['_token']);
        ProductModel::create($input);        
        return redirect('product')->with('flash_message' , "Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $arr = ProductModel::find($id);
        $categories_arr = DB::table('categories')->where('status','=',1)->get();
        $architecture_arr = DB::table('architects')->where('status','=',1)->get();

        return view('admin.product.show',compact('arr', 'categories_arr', 'architecture_arr'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $details = ProductModel::find($id);
        $input = $request->all();
        
        
        if(isset($input['image'])){
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('images/Products', $file_name, 'public');
            $input['image'] = "storage/images/Products/".$file_name;

            if(file_exists($input['old_img'])){
                unlink($input['old_img']);
            }
        }
        
        
        if(isset($input['otherImage']))
        {

            if($input['old_otherImage'] != 'null'){
                foreach(json_decode($input['old_otherImage']) as $old_other_img){
                    if(file_exists($old_other_img)){
                        unlink($old_other_img);
                    }
                }
            }

            

            $names = [];
            foreach($request->file('otherImage') as $image)
            {
                $destinationPath = 'storage/images/other_Products/';
                $filename = $destinationPath.time().$image->getClientOriginalName();
                $image->move($destinationPath, $filename);
                array_push($names, $filename);
            }

            $input['otherImage'] = json_encode($names);
        }
        
        request()->validate([
            'name' => 'required|max:200|min:3|regex:/^[A-Za-z_-]/', 
            'size' => 'required|max:20|min:3|', 
            'regular_price' => 'required|numeric', 
            'selling_price' => 'required|numeric',
            //'stock_amount' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
            'product_code' => 'required', 
            'product_materials' => 'required', 
            'architect_id' => 'required', 
            'category_id' => 'required'
        ]);
        

        unset($input['_token']);
        unset($input['old_img']);
        unset($input['old_otherImage']);
        unset($input['stock_amount']);

        $details->update($input);
        return redirect('product')->with('flash_message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = ProductModel::find($id);

        if(file_exists($data['image'])){
            unlink($data['image']);
        }

        if(file_exists($data['otherImage'])){
            foreach(json_decode($data['otherImage']) as $v){
                unlink($v);
            }
        }

        

        ProductModel::destroy($id);
        return redirect('product')->with('del_message', 'Deleted!');
    }
}
