<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function Laravel\Prompts\confirm;

class CartController extends Controller
{
    
    public function addToCart(Request $request){      
        $product = ProductModel::find($request->id);
        
        if($product == null){
            return response()->json([
                'status' => false,
                'message' => "Product not found"
            ]);
        }
        
        if(Cart::count() > 0){
            
            $cartContent = Cart::content();
            $productAlreadyExist = false;

            foreach($cartContent as $item){
                if($item->id == $product->id){
                    $productAlreadyExist = true;
                }
            }

            if($productAlreadyExist == false){
                Cart::add($product['id'], $product['name'], 1,$product['regular_price'], ['productImage' => $product['image']]);
                $status = true;
                $message = $product['name']." is added in cart";
            }else{
                $status = false;
                $message = $product['name']." is already added in cart";
            }
            
        }else{
            //Param Details: 1. Product ID, 2. Product Name, 3. Price, 4. Product Image, 5. Tax
            Cart::add($product['id'], $product['name'], 1,$product['regular_price'], ['productImage' => $product['image']], 0);            
            $status = true;
            $message = $product['name']." is added in cart";
        }

        return response()->json([
            'status' => $status,
            'message' => $message
        ]);

    }


    public function cart(){
        //dd(Cart::content());
        $cartContent = Cart::content();
        return view('frontend.cart', compact('cartContent'));
    }

    public function updateCart(Request $request){
        $rowId = $request->rowId;
        $qty = $request->qty;

        $itemInfo = Cart::get($rowId);

        $product = ProductModel::find($itemInfo->id);

        if($qty <= $product['stock_amount']){
            Cart::update($rowId, $qty);
            $message = "Cart updated successfully";
            $status = true;
            session()->flash('success', $message);
        }else{
            $message = "Requested quantity ($qty) not available in stock";
            $status = false;
            session()->flash('error', $message);
        }  

        
        return response()->json([
            'status' => $status,
            'message' => $message
        ]);
    }

    public function deleteItem(Request $request){        

        $rowId = $request->rowId;
        $itemInfo = Cart::get($rowId);

        if($itemInfo ==null){
            $message = "Item is not found in cart";
            session()->flash('error', $message);

            return response()->json([
                'status' => false,
                'message' => $message
            ]);
        }

        Cart::remove($request->rowId);

        $message = "Item is deleted from cart";
        session()->flash('success', $message);               
        return response()->json([              
            'status' => true,
            'message' => $message
        ]);
    }

    public function addToCartWithQty(Request $request){
        
        $product = ProductModel::find($request->id);
        
        if($product == null){
            return response()->json([
                'status' => false,
                'message' => "Product not found"
            ]);
        }
        
        if(Cart::count() > 0){
            
            $cartContent = Cart::content();
            $productAlreadyExist = false;

            foreach($cartContent as $item){
                if($item->id == $product->id){
                    $productAlreadyExist = true;
                }
            }

            if($productAlreadyExist == false){
                Cart::add($product['id'], $product['name'], 1,$product['regular_price'], ['productImage' => $product['image']]);
                $status = true;
                $message = $product['name']." is added in cart";
            }else{
                $status = false;
                $message = $product['name']." is already added in cart";
            }
            
        }else{
            //Param Details: 1. Product ID, 2. Product Name, 3. Price, 4. Product Image, 5. Tax
            Cart::add($product['id'], $product['name'], $request->qty,$product['regular_price'], ['productImage' => $product['image']], 0);            
            $status = true;
            $message = $product['name']." is added in cart";
        }

        return response()->json([
            'status' => $status,
            'message' => $message
        ]);
    }

    
    public function checkout(){

        if(Cart::count() == 0){
            return redirect()->route('front.cart');
        }

        // if(Auth::check() == false){
        //     return redirect()->route('customer.login');
        // }

        return view('frontend.checkout');
    }

}
