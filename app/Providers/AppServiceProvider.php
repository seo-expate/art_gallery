<?php

namespace App\Providers;

use App\Models\CategoryModel;
use App\Models\ProductModel;
use Illuminate\Support\ServiceProvider;
use View;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        View::composer(['frontend.layout'],function ($view){
            $view->with([
                'categories'=>CategoryModel::where('status', 1)->get(),
                
            ]);
        });

        View::composer(['frontend.layout'],function ($view){
            $view->with([
                'products'=> ProductModel::with('category', 'architect')->get(),
                
            ]);
        });
        
        

    }
}
