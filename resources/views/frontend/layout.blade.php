<?php
	// echo "<pre>";
	// print_r($categories[2]['id']);
	// echo "</pre>";die;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Art Design</title>
	<link rel="stylesheet" href="{{asset('assets/art_design/css/bootstrap.min.css')}}">	
	<link rel="stylesheet" href="{{asset('assets/art_design/css/project-1.css')}}">	
	<link rel="stylesheet" href="{{asset('assets/art_design/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/art_design/css/responsive.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
	<script src="{{asset('assets/art_design/js/jquery-3.7.1.min.js')}}"></script>
</head>

<body>	
	<!--===========================header part start===================================-->
	<header class="header_part">
		<div class="container superNavv">
			<div class="row">
				<div class="col-12">
					<div class="superNav border-bottom py-2 text-light">
				      <div class="container">
				        <div class="row">
				          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 centerOnMobile">
				            <select  class="me-3 border-0 bg-light">
				              	<option value="en-us">EN-US</option>
				              	<option value="en-us">EN-CA</option>
				              	<option value="en-us">EN-UK</option>
				            </select>
				            <span class="d-none d-lg-inline-block d-md-inline-block d-sm-inline-block d-xs-none me-3"><strong>info@something.com</strong></span>
				            <span class="me-3">
				            	<!-- <i class="fa-solid fa-phone me-1 text-warning"></i> --> 
				            	<span style="font-size: 16px; color: #fa8231; font-weight: 700;"> &#9990;</span>
				            	<strong>+8801-123-123440</strong>
				            </span>
				          </div>
				          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 d-none d-lg-block d-md-block-d-sm-block d-xs-none text-end">
				            <span class="me-3"><i class="fa-solid fa-truck text-light me-1"></i><a class="text-light" href="#">Shipping</a></span>
				            <span class="me-3"><i class="fa-solid fa-file  text-light me-2"></i><a class="text-light" href="#">Policy</a></span>
				          </div>
				        </div>
				      </div>
				    </div>
				    <nav class="navbar navbar-expand-lg bgdark sticky-top navbar-light shadow-sm ">
				      <div class="container ">
				        <a class="navbar-brand" href="/"><i class="fa-solid fa-shop me-2"></i> 
				        	<img src="{{asset('assets/art_design/images/comlogo.png')}}" alt="LOGO" width="100px" height="40px">				        
				        </a>
				        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				          <span class="navbar-toggler-icon"></span>
				        </button>
				    
				        <div class="mx-auto my-3 d-lg-none d-sm-block d-xs-block ">
				          <div class="input-group">
				            <span class="border-dark input-group-text text-white"><i class="fa-solid fa-magnifying-glass"></i></span>
				            <input type="text" class="form-control">
				            <button class="btn text-white">Search</button>
				          </div>
				        </div>
				        <div class=" collapse navbar-collapse" id="navbarNavDropdown">
				          <div class="ms-auto d-none d-lg-block">
				            <div class="input-group">
				              <span class="border-dark input-group-text text-white"><i class="fa-solid fa-magnifying-glass"></i></span>
				              <input type="text" class="form-control">
				              <button class="btn text-white">Search</button>
				            </div>
				          </div>
				          <ul class="navbar-nav ms-auto ">
				            <li class="nav-item">
				              <a class="nav-link mx-2 text-uppercase active" aria-current="page" href="/">Home</a>
				            </li>
				            <li class="nav-item">
				              <a class="nav-link mx-2 text-uppercase " aria-current="page" href="#">Offers</a>
				            </li>
				            <li class="nav-item dropdown dropdown-mega position-static">
					            <a class="nav-link mx-2 text-uppercase dropdown-toggle" href="#" data-bs-toggle="dropdown" data-bs-auto-close="outside"> Catalog</a>
					            <div class="dropdown-menu shadow dropdownmega">
					              <div class="mega-content px-4">
					                <div class="container-fluid">
					                  <div class="row">
									
									  
									  	@foreach($categories as $category)										  
					                    <div class="col-3 col-sm-3 col-md-3 py-3">
					                      <h5>{{ $category['name'] }}</h5>
					                      <div class="list-group">
										  	@foreach($products as $product)
												@if($category['id'] == $product['category_id'])
					                        	<a class="list-group-item" href="">{{ $product->name }}</a>
												@endif
												
											@endforeach
					                      </div>
					                    </div>

										@endforeach

					                    <!-- <div class="col-12 col-sm-4 col-md-3 py-4">
					                      	<h5>Coffee Makers</h5>
					                      	<div class="card">
							                  	<img src="{{asset('assets/art_design/images/7.jpg')}}" class="img-fluid" alt="image">
							                  	<div class="card-body">
							                    	<p>Coffee Makers</p>
							                  	</div>
							                </div>
					                    </div>

					                    <div class="col-12 col-sm-4 col-md-3 py-4">
					                      <h5>Stand Mixers</h5>
					                      <div class="list-group">
					                        <a class="list-group-item" href="#">Bowl-Lift Stand Mixers</a>
					                        <a class="list-group-item" href="#">Mixer Attachment</a>
					                        <a class="list-group-item" href="#">Tilt-Head Stand Mixers</a>
					                      </div>
					                    </div>

					                    <div class="col-12 col-sm-12 col-md-3 py-4">
					                      <h5>Espresso Machines</h5>
					                      <div class="list-group">
					                        <a class="list-group-item" href="#">Automatic Espresso</a>
					                        <a class="list-group-item" href="#">Hands-On Espresso</a>
					                        <a class="list-group-item" href="#">Manual Espresso Makers</a>
					                      </div>
					                    </div> -->

					                  </div>
					                </div>
					              </div>
					            </div>
					        </li>
				            <li class="nav-item">
				              <a class="nav-link mx-2 text-uppercase" href="{{ route('shop') }}">Shop</a>
				            </li>
				            <li class="nav-item">
				              <a class="nav-link mx-2 text-uppercase" href="#">Services</a>
				            </li>
				            <li class="nav-item">
				              <a class="nav-link mx-2 text-uppercase" href="#">About</a>
				            </li>
				          </ul>
				          <ul class="navbar-nav ms-auto ">
				            <li class="nav-item">
				              <a class="nav-link mx-2 text-uppercase" href="#">
				              	<!-- <i class="fa-regular fa-heart"></i> -->
				              	<span style="font-size:35px;">&#9825;</span>
				              </a>
				            </li>
				            <li class="nav-item">
				              <a class="nav-link mx-2 text-uppercase minicart-icon" href="javascript:void(0);" >
				              	<!-- <i class="fa-solid fa-cart-shopping "></i> -->
				              	<span style="font-size:30px;">&#128722;</span>
				              </a>
				              	<div class="cart-dropdown">
									<ul>
										<li>
											<div class="mini-cart-thumb">
												<a href="#"><img src="{{asset('assets/art_design/images/41.jpg')}}" alt="img" /></a>
											</div>
											<div class="mini-cart-heading">
												<span>TK 240 x 1</span>
												<h5><a href="#"> Paper Design</a></h5>
											</div>
											<div class="mini-cart-remove">
												<button><i class="ti-close"></i></button>
											</div>
										</li>
										<li>
											<div class="mini-cart-thumb">
												<a href="#"><img src="{{asset('assets/art_design/images/img (6).jpg')}}" alt="img" /></a>
											</div>
											<div class="mini-cart-heading">
												<span>TK 19.00 x 1</span>
												<h5><a href="#">Electric Kettle</a></h5>
											</div>
											<div class="mini-cart-remove">
												<button><i class="ti-close"></i></button>
											</div>
										</li>
									</ul>
									<div class="minicart-total justify-content-between d-flex">
										<span class="pull-left">total : </span>
										<span class="pull-right"> TK 259.00</span>
									</div>
									<div class="mini-cart-checkout">
										<a target="blank" href="cart.html" class="btn-common view-cart">VIEW CART</a>
										<a target="blank" href="" class="btn-common checkout mt-10">CHECK OUT</a>
									</div>
								</div>
				            </li>
				            <li class="nav-item">
				              <a class="nav-link mx-2 text-uppercase my_account" href="javascript:void(0);">
				              	<!-- <i class="fa-solid fa-circle-user"></i> -->
				              	<span style="font-size:30px;"> 👨‍🎓</span>
				              </a>
				              <div class="my_account_dropdown">
									<ul>
										<li><a href="{{ Session::get('role') == 2 ? '/customer_dashboard' : route('customer.index') }}">My Account</a></li>
										<li><a href="{{ route('customer.index') }}">User Login</a></li>
										<li><a href="{{ Session::get('role') == 2 ? '#' : '/customer_dashboard' }}">User Register </a></li>
									</ul>																		
								</div>
				            </li>
				          </ul>
				        </div>
				      </div>
				    </nav> 
				</div>
			</div>
		</div>
	</header> 
	<!--===========================header part end===================================-->
    @yield('content')
	<!--===========================footer part start===================================-->
	<footer class="footer_part pt-5 pb-5">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 contact_info">
					<div class="">
						<h5 class="mb-4">Contact Us</h5>
						<div class="address d-flex gap-3">
							<i class="fa-solid fa-location-dot"></i>
							<p>Majhira, Shajahanpur, Bogura</p>
						</div>
						<div class="phone d-flex gap-3">
							<i class="fa-solid fa-phone-volume"></i>
							<p>+8801928201238</p>
						</div>
						<div class="email d-flex gap-3">
							<i class="fa-solid fa-envelopes-bulk"></i>
							<p>lorem@gmail.com</p>
						</div>
					</div>
					<div class="subscribe">
						<h5>Subscribe Now</h5>
						<p>Subscribe to our newsletterget 10% off your first purchase at here for update</p>
						<input type="text">
					</div>
				</div>
				<div class="col-sm-3 contact_info">
					<div class="about_info ">
						<h5 class="mb-4">About</h5>
						<div class=" ">
							<p><a href="#">Contact Us</a></p>
						</div>
						<div class=" ">
							<p><a href="#">About Us</a></p>
						</div>
						<div class=" ">
							<p><a href="#">Careers</a></p>
						</div>
						<div class=" ">
							<p><a href="#">Flipkart Stories</a></p>
						</div>
						<div class="">
							<p><a href="#">Press</a></p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 contact_info">
					<div class="about_info ">
						<h5 class="mb-4">Help</h5>
						<div class=" ">
							<p><a href="#">Payments </a></p>
						</div>
						<div class=" ">
							<p><a href="#">Shipping </a></p>
						</div>
						<div class=" ">
							<p><a href="#">Cancellation </a></p>
						</div>
						<div class=" ">
							<p><a href="#">FAQ </a></p>
						</div>
						<div class="">
							<p><a href="#">Report Infringement</a></p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 ">
					<div class="about_info ">
						<h5 class="mb-4">Policy</h5>
						<div class=" ">
							<p><a href="#">Return Policy </a></p>
						</div>
						<div class=" ">
							<p><a href="#">Terms Of Use </a></p>
						</div>
						<div class=" ">
							<p><a href="#">Security  </a></p>
						</div>
						<div class=" ">
							<p><a href="#">Privacy  </a></p>
						</div>
						<div class="">
							<p><a href="#">Sitemap</a></p>
						</div>
					</div>
				</div>
			</div>
			<div class="row text-center pt-3 mt-5 footer_bottom">
				<div class="col-12">
					<p>Copyright © 2024 seoexpate </p>
				</div>
			</div>
		</div>
	</footer>
	<!--===========================footer part end===================================-->

	<script src="{{asset('assets/art_design/js/bootstrap.bundle.min.js')}}"></script>
	<script src="{{asset('assets/art_design/js/main.js')}}"></script>

</body>
</html>
