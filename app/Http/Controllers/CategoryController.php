<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $arr = CategoryModel::all();
        return view('admin.category.index',compact('arr'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //dd($input);
        request()->validate([
            'name' => 'required|max:200|min:3|regex:/^[A-Za-z_-]/',
        ]);


        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('images/Categories', $file_name, 'public');
            $input['image'] = "storage/images/Categories/".$file_name;
        }else{
            $input['image'] = '';
        }

        unset($input['_token']);
        CategoryModel::create($input);        
        return redirect('category')->with('flash_message' , "Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $arr = CategoryModel::find($id);
        return view('admin.category.show',compact('arr'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $details = CategoryModel::find($id);
        $input = $request->all();
        
        if(isset($input['image'])){
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('images/Categories', $file_name, 'public');
            $input['image'] = "storage/images/Categories/".$file_name;

            if(file_exists($input['old_img'])){
                unlink($input['old_img']);
            }

        }        

        request()->validate([
            'name' => 'required|max:100|min:3|regex:/^[A-Za-z_-]/', 
        ]);

        unset($input['_token']);
        unset($input['old_img']);

        $details->update($input);
        return redirect('category')->with('flash_message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = CategoryModel::find($id);

        if(file_exists($data['image'])){
            unlink($data['image']);
        }
        
        CategoryModel::destroy($id);
        return redirect('category')->with('del_message', 'Deleted!');
    }
}
