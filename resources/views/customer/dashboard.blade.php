@extends('customer.layout')
@section('content')


<main id="main" class="main">
    <div class="pagetitle">
    @if(Session::has('flash_message'))
      <br><br>
      <div class="alert alert-success">
          <h5>{{ Session::get('flash_message') }}</h5>
      </div>
    @endif

    @if(Session::has('error'))
        <br><br>
        <div class="alert alert-danger">
            <h5>{{ Session::get('error') }}</h5>
        </div>
    @endif
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ Session::get('role') == 2 ? '/customer_dashboard' : '/dashboard' }}">Home</a></li>
          <li class="breadcrumb-item">Users</li>
          <li class="breadcrumb-item active">Profile</li>
        </ol>
      </nav>
    </div>

    <section class="section profile">
      <div class="row">
        <div class="col-xl-4">

          <div class="card">
            <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

              <img src="{{ file_exists($data['image'])? asset($data['image']) : asset('storage/images/dummy-image.jpg') }}" alt="Profile" class="rounded-circle">
              <h2>{{ $data['name'] }}</h2>
              <!-- <h3>Web Designer</h3> -->
              <div class="social-links mt-2">
                <a href="{{ $data['twitter'] }}" class="twitter"><i class="bi bi-twitter"></i></a>
                <a href="{{ $data['facebook'] }}" class="facebook"><i class="bi bi-facebook"></i></a>
                <a href="{{ $data['instagram'] }}" class="instagram"><i class="bi bi-instagram"></i></a>
                <a href="{{ $data['linkedIn'] }}" class="linkedin"><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
          </div>

        </div>

        <div class="col-xl-8">

          <div class="card">
            <div class="card-body pt-3">
              <!-- Bordered Tabs -->
              <ul class="nav nav-tabs nav-tabs-bordered">

                <li class="nav-item">
                  <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button>
                </li>

                <li class="nav-item">
                  <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
                </li>

                <li class="nav-item">
                  <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password">Change Password</button>
                </li>

              </ul>
              <div class="tab-content pt-2">

                <div class="tab-pane fade show active profile-overview" id="profile-overview">

                  <h5 class="card-title">Profile Details</h5>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label ">Full Name</div>
                    <div class="col-lg-9 col-md-8">{{ $data['name'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Phone</div>
                    <div class="col-lg-9 col-md-8">{{ $data['phone'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Email</div>
                    <div class="col-lg-9 col-md-8">{{ $data['email'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Date of Birth</div>
                    <div class="col-lg-9 col-md-8">{{ $data['date_of_birth'] }}</div>
                  </div>
                  
                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Gender</div>
                    <div class="col-lg-9 col-md-8">{{ $data['gender'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Address</div>
                    <div class="col-lg-9 col-md-8">{{ $data['street_address'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">City</div>
                    <div class="col-lg-9 col-md-8">{{ $data['city'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">State</div>
                    <div class="col-lg-9 col-md-8">{{ $data['state'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Post</div>
                    <div class="col-lg-9 col-md-8">{{ $data['post'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Country</div>
                    <div class="col-lg-9 col-md-8">{{ $data['country'] }}</div>
                  </div>
                  
                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">SSN</div>
                    <div class="col-lg-9 col-md-8">{{ $data['ssn'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Company</div>
                    <div class="col-lg-9 col-md-8">{{ $data['company'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Website</div>
                    <div class="col-lg-9 col-md-8">{{ $data['website'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Facebook</div>
                    <div class="col-lg-9 col-md-8">{{ $data['facebook'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">linkedIn</div>
                    <div class="col-lg-9 col-md-8">{{ $data['linkedIn'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">twitter</div>
                    <div class="col-lg-9 col-md-8">{{ $data['twitter'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">youtube</div>
                    <div class="col-lg-9 col-md-8">{{ $data['youtube'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">instagram</div>
                    <div class="col-lg-9 col-md-8">{{ $data['instagram'] }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Marital Status</div>
                    <div class="col-lg-9 col-md-8">
                      
                        @if($data['marital_status'] == '1')
                          Single
                        @elseif($data['marital_status'] == '2')
                          Married
                        @elseif($data['marital_status'] == '3')
                          Divorced
                        @else
                          
                        @endif
                    
                    </div>
                  </div>
                  

                </div>

                <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

                  <!-- Profile Edit Form -->
                  <form method="post" action="{{ url('customer/'.$data['id']) }}" enctype="multipart/form-data">
                  {!! csrf_field() !!}
                  {{ method_field('PUT') }}
                  <input type="hidden" name="old_img" value="{{ file_exists($data['image'])? $data['image'] : '' }}">
                  <div class="row mb-3">
                    <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Change Profile Image</label>
                    <div class="col-md-8 col-lg-9">

                      <img src="{{ file_exists($data['image'])? asset($data['image']) : asset('storage/images/dummy-image.jpg') }}" alt="Profile" class="img-fluid">

                      <div class="pt-2">
                        <input name="image" type="file" class="form-control" id="profileImage">
                      </div>
                    </div>
                  </div>
                    

                    <div class="row mb-3">
                      <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Full Name</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="name" type="text" class="form-control" id="fullName"  value="{{ $data['name'] }}">
                        <b><small class="text-danger">{{ $errors->first('name') }}</small></b>
                      </div>
                    </div>
                    

                    <div class="row mb-3">
                      <label for="phone" class="col-md-4 col-lg-3 col-form-label">Phone</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="phone" type="text" class="form-control" id="phone" value="{{ $data['phone'] }}">
                        <b><small class="text-danger">{{ $errors->first('phone') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="email" type="text" class="form-control" id="email" value="{{ $data['email'] }}">
                        <b><small class="text-danger">{{ $errors->first('email') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="date_of_birth" class="col-md-4 col-lg-3 col-form-label">Date of Birth</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="date_of_birth" type="date" class="form-control" id="date_of_birth" value="{{ $data['date_of_birth'] }}">
                        <b><small class="text-danger">{{ $errors->first('date_of_birth') }}</small></b>
                      </div>
                    </div> 

                    <div class="row mb-3">
                      <label for="gender" class="col-md-4 col-lg-3 col-form-label">Gender</label>
                      <div class="col-md-8 col-lg-9">
                        <select name="gender" class="form-select">
                          <option value="">Select</option>
                          <option value="Male" <?= ($data['gender'] == 'Male')? 'Selected' : '' ?>>Male</option>
                          <option value="Famale" <?= ($data['gender'] == 'Famale')? 'Selected' : '' ?>>Famale</option>
                        </select>
                        <b><small class="text-danger">{{ $errors->first('gender') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="address" class="col-md-4 col-lg-3 col-form-label">Address</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="street_address" type="text" class="form-control" id="address" value="{{ $data['street_address'] }}">
                        <b><small class="text-danger">{{ $errors->first('street_address') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="city" class="col-md-4 col-lg-3 col-form-label">City</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="city" type="text" class="form-control" id="city" value="{{ $data['city'] }}">
                        <b><small class="text-danger">{{ $errors->first('city') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="state" class="col-md-4 col-lg-3 col-form-label">State</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="state" type="text" class="form-control" id="state" value="{{ $data['state'] }}">
                        <b><small class="text-danger">{{ $errors->first('state') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="post" class="col-md-4 col-lg-3 col-form-label">Post Code</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="post" type="text" class="form-control" id="post" value="{{ $data['post'] }}">
                        <b><small class="text-danger">{{ $errors->first('post') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="country" class="col-md-4 col-lg-3 col-form-label">Country</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="country" type="text" class="form-control" id="country" value="{{ $data['country'] }}">
                        <b><small class="text-danger">{{ $errors->first('country') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="ssn" class="col-md-4 col-lg-3 col-form-label">SSN</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="ssn" type="text" class="form-control" id="ssn" value="{{ $data['ssn'] }}">
                        <b><small class="text-danger">{{ $errors->first('ssn') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="company" class="col-md-4 col-lg-3 col-form-label">Comapny</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="company" type="text" class="form-control" id="company" value="{{ $data['company'] }}">
                        <b><small class="text-danger">{{ $errors->first('company') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="website" class="col-md-4 col-lg-3 col-form-label">Website</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="website" type="text" class="form-control" id="website" value="{{ $data['website'] }}">
                        <b><small class="text-danger">{{ $errors->first('website') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="facebook" class="col-md-4 col-lg-3 col-form-label">Facebook</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="facebook" type="text" class="form-control" id="facebook" value="{{ $data['facebook'] }}">
                        <b><small class="text-danger">{{ $errors->first('facebook') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="linkedIn" class="col-md-4 col-lg-3 col-form-label">LinkedIn</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="linkedIn" type="text" class="form-control" id="linkedIn" value="{{ $data['linkedIn'] }}">
                        <b><small class="text-danger">{{ $errors->first('linkedIn') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="twitter" class="col-md-4 col-lg-3 col-form-label">Twitter</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="twitter" type="text" class="form-control" id="twitter" value="{{ $data['twitter'] }}">
                        <b><small class="text-danger">{{ $errors->first('twitter') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="youtube" class="col-md-4 col-lg-3 col-form-label">Youtube</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="youtube" type="text" class="form-control" id="youtube" value="{{ $data['youtube'] }}">
                        <b><small class="text-danger">{{ $errors->first('youtube') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="instagram" class="col-md-4 col-lg-3 col-form-label">Instagram</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="instagram" type="text" class="form-control" id="instagram" value="{{ $data['instagram'] }}">
                        <b><small class="text-danger">{{ $errors->first('instagram') }}</small></b>
                      </div>
                    </div>
                    

                    <div class="row mb-3">
                      <label for="marital_status" class="col-md-4 col-lg-3 col-form-label">Marital Status</label>
                      <div class="col-md-8 col-lg-9">
                        <select name="marital_status" class="form-select">
                          <option value="">Select</option>
                          <option value="1" <?= ($data['marital_status'] == '1')? 'Selected' : '' ?>>Single</option>
                          <option value="2" <?= ($data['marital_status'] == '2')? 'Selected' : '' ?>>Married</option>
                          <option value="3" <?= ($data['marital_status'] == '3')? 'Selected' : '' ?>>Divorced</option>
                        </select>
                        <b><small class="text-danger">{{ $errors->first('marital_status') }}</small></b>
                      </div>
                    </div> 
                    

                    <div class="text-center">
                      <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                  </form><!-- End Profile Edit Form -->

                </div>

                <div class="tab-pane fade pt-3" id="profile-change-password">
                  <!-- Change Password Form -->
                  <form method="post" action="{{ url('customer/'.$data['id']) }}">
                  {!! csrf_field() !!}
                  <input type="hidden" name='id' value="{{ $data['id'] }}">
                  {{ method_field('PUT') }}
                    <div class="row mb-3">
                      <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Current Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="current_password" type="password" class="form-control" id="currentPassword">
                        <b><small class="text-danger">{{ $errors->first('current_password') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">New Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="new_password" type="password" class="form-control" id="newPassword">
                        <b><small class="text-danger">{{ $errors->first('new_password') }}</small></b>
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Re-enter New Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="new_password_confirmation" type="password" class="form-control" id="renewPassword">
                        <b><small class="text-danger">{{ $errors->first('new_password_confirmation') }}</small></b>
                      </div>
                    </div>

                    <div class="text-center">
                      <button type="submit" class="btn btn-primary">Change Password</button>
                    </div>
                  </form><!-- End Change Password Form -->

                </div>

              </div><!-- End Bordered Tabs -->

            </div>
          </div>

        </div>
      </div>
    </section>
    

</main>
@endsection