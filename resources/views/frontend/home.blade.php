@extends('frontend.layout')
@section('content')

<section class="top-slider">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div id="carouselExampleDark" class="carousel carousel-dark slide">
					  <div class="carousel-indicators">
					    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
					    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
					    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
					  </div>
					  <div class="carousel-inner">
					    <div class="carousel-item active" data-bs-interval="10000">
					      <img src="{{asset('assets/art_design/images/bg-1 (11).jpg')}}" class="d-block w-100" alt="img">
					      <div class="carousel-caption d-none d-md-block">
					        <h5>Summer Sale</h5>
					        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
					        <button type="button" class="btn btn">Shop Now</button>
					      </div>
					    </div>
					    <div class="carousel-item" data-bs-interval="2000">
					      <img src="{{asset('assets/art_design/images/bg-1 (9).jpg')}}" class="d-block w-100" alt="img">
					      <div class="carousel-caption d-none d-md-block">
					        <h5>Summer Sale</h5>
					        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
					        <button type="button" class="btn btn">Shop Now</button>
					      </div>
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('assets/art_design/images/bg-1 (10).jpg')}}" class="d-block w-100" alt="img">
					      <div class="carousel-caption d-none d-md-block">
					        <h5>Summer Sale</h5>
					        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
					        <button type="button" class="btn btn">Shop Now</button>
					      </div>
					    </div>
					  </div>
					  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="visually-hidden">Previous</span>
					  </button>
					  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="visually-hidden">Next</span>
					  </button>
					 </div>
				</div>
			</div>
		</div>
	</section>
	<!--===========================slider part end===================================-->

	<!--===========================banner part start===================================-->
	<section class="banner">
		<div class="container">
			<div class="row bannerpart">
				<div class="col-md-6 pt-3 bannerpartimg">
					<div class="bannerimage">
						<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/bg-1 (13).jpg')}}" alt="img" class="d-block w-100"></a>
						<div class="bannerimage_text ">
							<h3>Flower Pot</h3>
							<p>Now in all Color Varient Available..</p>
							<button class="btn" type="button">Shop Now</button>
						</div>
					</div>
				</div>
				<div class="col-md-6 pt-3 bannerpartimg">
					<div class="bannerimage">
						<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/bg-1 (12).jpg')}}" alt="img" class="d-block w-100"></a>
						<div class="bannerimage_text ">
							<h3>Electric Kettle</h3>
							<p>Now in all Color Varient Available..</p>
							<button class="btn" type="button">Shop Now</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--===========================banner part end===================================-->

	<!--===========================Our Best Collection part start===================================-->
					
	<section class="our_best_collection">
		<div class="container">
	      <div class="row mt-5" id="filter-buttons">
	      	<h2>Our Best Collection</h2>
	        <div class="col-12 text-center mt-4">
	          <button class="btn mb-2 me-1 active" data-filter="all">Show all</button>
	          <button class="btn mb-2 mx-1" data-filter="Coffee">Coffee Makers</button>
	          <button class="btn mb-2 mx-1" data-filter="Kettles">Kettles</button>
	          <button class="btn mb-2 mx-1" data-filter="Slicer">Slicer</button>
	        </div>
	      </div>
	      <div class="row px-2 mt-4 gap-3" id="filterable-cards">
	        <div class="card p-0" data-name="Coffee">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (1).jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	        <div class="card p-0" data-name="Coffee">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (2).jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	        <div class="card p-0" data-name="Coffee">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (3).jpg')}}" alt="img"></a>
	        	</div>		          
	          <div class="card-body">
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	        <div class="card p-0" data-name="Kettles">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (6).jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	        <div class="card p-0" data-name="Kettles">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/11.jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">
	           	<div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	        <div class="card p-0" data-name="Kettles">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (5).jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	        <div class="card p-0" data-name="Kettles">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (7).jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	        <div class="card p-0" data-name="Slicer">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (9).jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	       	<div class="card p-0" data-name="Slicer">
	       		<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/2.jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	        <div class="card p-0" data-name="Slicer">
	        	<div class="card-header">
	        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/img (10).jpg')}}" alt="img"></a>
	        	</div>	          
	          <div class="card-body">	            
	            <div class="rating  col-md-12 col-xl-12">
	            	<p>100$</p>
				    <div class="rat col-md- col-xl-">
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star"></i>
				        <i class="fa-solid fa-star-half-stroke"></i>				        
				        <i class="fa-regular fa-star"></i>
				    </div>
				    <p>laborum et dolorum fug </p>
				    <p>Lorem ipsum, dolor sit amet.</p>
				</div>
	          </div>
	          <div class="woocommerce_cart">
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-cart-plus"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-regular fa-heart"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<i class="fa-solid fa-layer-group"></i>
	          	</div>
	          	<div class="woo_cart">
	          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
	          	</div>
	          </div>
	        </div>
	      </div>
	    </div>
	</section>

	<!--===========================Our Best Collection part end===================================-->

	<!--===========================Bowl Designs Products part start===================================-->
	<section class="Bowl_Designs">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="bowldiv">
						<div class="bowl_img">
							<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/14.jpg')}}" alt="img" class="img-fluid"></a>
						</div>
						<div class="bowl_text ">
							<h3>Clay Bowl Designs</h3>
							<p>Now Available all over the world</p>
							<button class="btn" type="button">Shop Now</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--===========================Bowl Designs Products part end===================================-->

	<!--===========================Bestseller Products part start===================================-->
	<section class="Bestseller_products">
		<div class="container text-center my-3">
		    <h2 class=" pt-5 pb-5">Bestseller Products</h2>
		    <div class="row mx-auto my-auto justify-content-center">
		        <div id="recipeCarousel" class="carousel slide" data-bs-ride="carousel">
		            <div class="carousel-inner" role="listbox">
		                <div class="carousel-item active carouselitem">
		                    <div class="col-md-3">
		                        <div class="card p-0">
						        	<div class="card-header">
						        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/10.jpg')}}" alt="img"></a>
						        	</div>	          
						          <div class="card-body">
						            <div class="rating  col-md-12 col-xl-12">
						            	<p>100$</p>
									    <div class="rat col-md- col-xl-">
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star-half-stroke"></i>				        
									        <i class="fa-regular fa-star"></i>
									    </div>
									    <p>laborum et dolorum fug </p>
									    <p>Lorem ipsum, dolor sit amet.</p>
									</div>
						          </div>
						          <div class="woocommerce_cart">
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-cart-plus"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-regular fa-heart"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-layer-group"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
						          	</div>
						          </div>
						        </div>
		                    </div>
		                </div>
		                <div class="carousel-item carouselitem">
		                    <div class="col-md-3">
		                        <div class="card p-0">
						        	<div class="card-header">
						        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/12.jpg')}}" alt="img"></a>
						        	</div>	          
						          <div class="card-body">
						            <div class="rating  col-md-12 col-xl-12">
						            	<p>200$</p>
									    <div class="rat col-md- col-xl-">
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star-half-stroke"></i>				        
									        <i class="fa-regular fa-star"></i>
									    </div>
									    <p>laborum et dolorum fug </p>
									    <p>Lorem ipsum, dolor sit amet.</p>
									</div>
						          </div>
						          <div class="woocommerce_cart">
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-cart-plus"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-regular fa-heart"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-layer-group"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
						          	</div>
						          </div>
						        </div>
		                    </div>
		                </div>
		                <div class="carousel-item carouselitem">
		                    <div class="col-md-3">
		                        <div class="card p-0" >
						        	<div class="card-header">
						        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/13.jpg')}}" alt="img"></a>
						        	</div>	          
						          <div class="card-body">
						            <div class="rating  col-md-12 col-xl-12">
						            	<p>300$</p>
									    <div class="rat col-md- col-xl-">
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star-half-stroke"></i>				        
									        <i class="fa-regular fa-star"></i>
									    </div>
									    <p>laborum et dolorum fug </p>
									    <p>Lorem ipsum, dolor sit amet.</p>
									</div>
						          </div>
						          <div class="woocommerce_cart">
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-cart-plus"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-regular fa-heart"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-layer-group"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
						          	</div>
						          </div>
						        </div>
		                    </div>
		                </div>
		                <div class="carousel-item carouselitem">
		                    <div class="col-md-3">
		                        <div class="card p-0" >
						        	<div class="card-header">
						        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/12.jpg')}}" alt="img"></a>
						        	</div>	          
						          <div class="card-body">
						            <div class="rating  col-md-12 col-xl-12">
						            	<p>400$</p>
									    <div class="rat col-md- col-xl-">
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star-half-stroke"></i>				        
									        <i class="fa-regular fa-star"></i>
									    </div>
									    <p>laborum et dolorum fug </p>
									    <p>Lorem ipsum, dolor sit amet.</p>
									</div>
						          </div>
						          <div class="woocommerce_cart">
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-cart-plus"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-regular fa-heart"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-layer-group"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
						          	</div>
						          </div>
						        </div>
		                    </div>
		                </div>
		                <div class="carousel-item carouselitem">
		                    <div class="col-md-3">
		                        <div class="card p-0" >
						        	<div class="card-header">
						        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/10.jpg')}}" alt="img"></a>						        	</div>	          
						          <div class="card-body">
						            <div class="rating  col-md-12 col-xl-12">
						            	<p>500$</p>
									    <div class="rat col-md- col-xl-">
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star-half-stroke"></i>				        
									        <i class="fa-regular fa-star"></i>
									    </div>
									    <p>laborum et dolorum fug </p>
									    <p>Lorem ipsum, dolor sit amet.</p>
									</div>
						          </div>
						          <div class="woocommerce_cart">
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-cart-plus"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-regular fa-heart"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-layer-group"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
						          	</div>
						          </div>
						        </div>
		                    </div>
		                </div>
		                <div class="carousel-item carouselitem">
		                    <div class="col-md-3">
		                        <div class="card p-0">
						        	<div class="card-header">
						        		<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/13.jpg')}}" alt="img"></a>
						        	</div>	          
						          <div class="card-body">
						            <div class="rating  col-md-12 col-xl-12">
						            	<p>600$</p>
									    <div class="rat col-md- col-xl-">
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star"></i>
									        <i class="fa-solid fa-star-half-stroke"></i>				        
									        <i class="fa-regular fa-star"></i>
									    </div>
									    <p>laborum et dolorum fug </p>
									    <p>Lorem ipsum, dolor sit amet.</p>
									</div>
						          </div>
						          <div class="woocommerce_cart">
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-cart-plus"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-regular fa-heart"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<i class="fa-solid fa-layer-group"></i>
						          	</div>
						          	<div class="woo_cart">
						          		<a href="product_details_page.html"><i class="fa-solid fa-eye"></i></a>
						          	</div>
						          </div>
						        </div>
		                    </div>
		                </div>
		            </div>
		            <a class="carousel-control-prev bg-transparent w-aut" href="#recipeCarousel" role="button" data-bs-slide="prev">
		                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		            </a>
		            <a class="carousel-control-next bg-transparent w-aut" href="#recipeCarousel" role="button" data-bs-slide="next">
		                <span class="carousel-control-next-icon" aria-hidden="true"></span>
		            </a>
		        </div>
		    </div>
		</div>
	</section>
	<!--===========================Bestseller Products part end===================================-->

	<!--===========================double_product Products part start===================================-->
	<section class="double_product pb-5 pt-5">
		<div class="container">
			<div class="row">
				<div class="col-md-6 doubleproduct prodouble">
					<div class="handmadepot">
						<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/bg-1 (9).jpg')}}" alt="img"></a>
						<div class="handmadepot_text ">
							<h3>Dinnerware Sets</h3>
							<p>Now in Many Design Available.</p>
							<button class="btn" type="button">Shop Now</button>
						</div>
					</div>
				</div>
				<div class="col-md-6 prodouble">
					<div class="handmadepot">
						<a href="product_details_page.html"><img src="{{asset('assets/art_design/images/bg-1 (10).jpg')}}" alt="img"></a>
						<div class="handmadepot_text ">
							<h3>Handmade Pots</h3>
							<p>Now in Many Different Color Available.</p>
							<button class="btn" type="button">Shop Now</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--===========================double_product Products part end===================================-->

	<!--===========================gallery Products part start===================================-->
	<section class="gallery_product">
		<div class="container">
			<h3>Trending style</h3>
			<h2>New Products</h2>
		 <div class="row">
		  <div class="col-lg-3 col-md-12 mb-4 mb-lg-0">
		    <a href="product_details_page.html"><img src="{{asset('assets/art_design/images//33.jpg')}}" alt="img" class="w-100 shadow-1-strong rounded mb-4"></a>
		    <a href="product_details_page.html"><img src="{{asset('assets/art_design/images/34.jpg')}}" class="w-100 shadow-1-strong rounded mb-4"alt="img" /></a>
		  </div>

		  <div class="col-lg-3 mb-4 mb-lg-0">
		    <a href="product_details_page.html"><img src="{{asset('assets/art_design/images/35.jpg')}}" alt="img" class="w-100 shadow-1-strong rounded mb-4"></a>
		    <a href="product_details_page.html"><img src="{{asset('assets/art_design/images/36.jpg')}}" class="w-100 shadow-1-strong rounded mb-4"alt="img" /></a>
		  </div>

		  <div class="col-lg-3 mb-4 mb-lg-0">
		    <a href="product_details_page.html"><img src="{{asset('assets/art_design/images/41.jpg')}}" alt="img" class="w-100 shadow-1-strong rounded mb-4"></a>
		    <a href="product_details_page.html"><img src="{{asset('assets/art_design/images/38.jpg')}}" class="w-100 shadow-1-strong rounded mb-4"alt="img" /></a>
		  </div>
		  <div class="col-lg-3 mb-4 mb-lg-0">
		    <a href="product_details_page.html">
		    	<img src="{{asset('assets/art_design/images/39.jpg')}}" alt="img" class="w-100 shadow-1-strong rounded mb-4">
		    </a>
		    <a href="product_details_page.html"><img src="{{asset('assets/art_design/images/40.jpg')}}" class="w-100 shadow-1-strong rounded mb-4"alt="img" /></a>
		  </div>
		 </div>
		</div>
	</section>
	<!--===========================gallery Products part end===================================-->

	<!--===========================client_logo Products part start===================================-->
	<section class="client_logo text-center pb-5">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<img src="{{asset('assets/art_design/images/Brand-2.png')}}" alt="img">
				</div>
				<div class="col-sm-3">
					<img src="{{asset('assets/art_design/images/Brand-3.png')}}" alt="img">
				</div>
				<div class="col-sm-3">
					<img src="{{asset('assets/art_design/images/Brand-1.png')}}" alt="img">
				</div>
				<div class="col-sm-3">
					<img src="{{asset('assets/art_design/images/Brand-4.png')}}" alt="img">
				</div>
			</div>
		</div>
	</section>
	<!--===========================client_logo Products part end===================================-->

@endsection